﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace tcpListener
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            TestConnection();
            Console.ReadLine();
            return;
            */
            string FS = "\u001c";
            try
            {
                IPAddress ipAddress = IPAddress.Parse("192.168.11.135");

                Console.WriteLine("Starting TCP listener...");

                TcpListener listener = new TcpListener(ipAddress, 2200);
                
                listener.Start();

                while (true)
                {
                    Console.WriteLine("Server is listening on " + listener.LocalEndpoint);

                    Console.WriteLine("Waiting for a connection...");

                    Socket client = listener.AcceptSocket();

                    Console.WriteLine("Connection accepted.");

                    Console.WriteLine("Reading data...");

                    byte[] data = new byte[100];
                    int size = client.Receive(data);
                    Console.WriteLine("Recieved data: ");
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < size; i++)
                        sb.Append(Convert.ToChar(data[i]));

                    string req = sb.ToString();
                    Console.WriteLine(req);

                    //req = Convert.ascc req.Substring(2);

                    string[] reqData = req.Split('\u001c');

                    ShowData(reqData);

                    string respData = "";

                    //respData = sb.ToString().Substring(2, 15) + "\u001c11000";

                    respData += FS + "15" + "60";
                    respData += FS + "09" + "02";
                    respData += FS + "13" + "01";
                    respData += FS + "01" + "10000";
                    respData += FS + "72" + "00000";

                    byte[] resp = Encoding.ASCII.GetBytes(respData);

                    List<byte> a = new List<byte> ();
                    a.AddRange(resp);
                    a.Insert(0,Convert.ToByte( respData.Length + 2));
                    a.Insert(0, 0);
                    resp = a.ToArray();

                    sb = new StringBuilder();
                    for (int i = 0; i < resp.Length; i++)
                        sb.Append(Convert.ToChar(resp[i]));
                    ShowData(sb.ToString().Split('\u001c'));

                    int ack  =  client.Send(resp);

                    Console.WriteLine();
                    client.Close();
                }

                listener.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.StackTrace);
                Console.ReadLine();
            }
        }

        public static void ShowData(string[] reqData)
        {
            for (int i = 0; i < reqData.Length; i++)
            {
                if (reqData[i].ToString().Length > 2)
                    Console.WriteLine(reqData[i].Substring(0, 2) + " " + reqData[i].Substring(2));
                else
                    Console.WriteLine(reqData[i]);
            }
        }

        public static void TestConnection()
        {
            try
            {
                // connect with a 5 second timeout on the connection
                TcpClient connection = new TcpClientWithTimeout("192.168.11.246", 2200, 5000).Connect();
                NetworkStream stream = connection.GetStream();

                // Send 10 bytes
                byte[] to_send = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xa };
                stream.Write(to_send, 0, to_send.Length);

                // Receive 10 bytes
                byte[] readbuf = new byte[10]; // you must allocate space first
                stream.ReadTimeout = 10000; // 10 second timeout on the read
                stream.Read(readbuf, 0, 10); // read

                // Disconnect nicely
                stream.Close(); // workaround for a .net bug: http://support.microsoft.com/kb/821625
                connection.Close();
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
    }
}
