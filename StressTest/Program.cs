﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Threading;
using System.Net;

namespace StressTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int sys_id = 0;
            int ts_id = 0;
            int agent_id = 0;
            int numOrders = 0;
            Console.WriteLine("Enter Tested System ID. 1-99");
            sys_id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Test Sequence ID. 0000-9999");
            ts_id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Test Agent ID. 01-99");
            agent_id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter number of orders. Max 9999");
            numOrders = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Stress test will be started with " + numOrders + " orders");
            Console.WriteLine("Press ENTER to continue");
            string answer = Console.ReadLine();
            if (answer != "")
                return;

            DoTest(sys_id, ts_id, agent_id, numOrders);

            Console.ReadLine();
         }

        static void DoTest(int sys_id, int seq_id, int agent_id, int txn_num )
        {
            string fileName = @"C:\Users\safaa.al-ledani\OneDrive - Leon's Furniture\Desktop\JSONS\Leons\712088813611_stress_test.json";
            string json = File.ReadAllText(fileName);
            for (int i = 1; i <= txn_num; i++)
            {
                string order_id = sys_id.ToString().PadLeft(2, '0') + seq_id.ToString().PadLeft(4, '0') + agent_id.ToString().PadLeft(2, '0') + i.ToString().PadLeft(4, '0').ToString();
                json = json.Replace("xxxxxxxxxxxx", order_id);
                json = json.Replace("first_name\":\"geethu\",\"last_name\":\"john", "first_name\":\"gee" + order_id + "\",\"last_name\":\"john" + order_id);
                Console.WriteLine("Sending order number: " + order_id + " " + i.ToString() + " of " + txn_num.ToString());
                //SendRequest(json);
            }

        }

        static void SendRequest(string json)
        {
            if (string.IsNullOrEmpty(json))
                return;

            string str = "";
            string result = "";
            System.Net.WebClient webClient = new System.Net.WebClient();
            JsonSerializer jsr = new JsonSerializer();
            JsonReader reader = new JsonReader(new StringReader(json));
            Object jsonObject = jsr.Deserialize(reader);
            if (jsonObject != null)
            {
                StringWriter sWriter = new StringWriter();
                JsonWriter writer = new JsonWriter(sWriter);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 4;
                writer.IndentChar = ' ';
                jsr.Serialize(writer, jsonObject);
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;
                str = sWriter.ToString();
                result = webClient.UploadString("http://10.128.2.40/SKUAvailSrv/SKUAvailSvc.svc/PostSaleRequest/", "POST", str);
                //Show results
                JsonSerializer s = new JsonSerializer();
                reader = new JsonReader(new StringReader(result));
                jsonObject = s.Deserialize(reader);
                if (jsonObject != null)
                {
                    sWriter = new StringWriter();
                    writer = new JsonWriter(sWriter);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 4;
                    writer.IndentChar = ' ';
                    s.Serialize(writer, jsonObject);
                    Console.WriteLine(sWriter.ToString());
                }
            }
        }
    }
}
