using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Net.NetworkInformation;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Security.Cryptography;

namespace EPocalipse.Json.JsonView
{
    public partial class MainForm : Form
    {
        private ColumnHeader SortingColumn = null;
        public MainForm()
        {
            InitializeComponent();
            cboRows.SelectedIndex = 2;
            //cboStatus.SelectedIndex = 2;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            dtFrom.Value = DateTime.Today;
            tmFrom.Value = DateTime.Now;
            tmTo.Value = DateTime.Now.AddHours(1);
            tmFrom.Checked = false;
            tmTo.Checked = false;
        }

        /// <summary>
        /// Closes the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item File > Exit</remarks>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Open File Dialog  for Yahoo! Pipe files or JSON files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item File > Open</remarks>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter =
               "Yahoo! Pipe files (*.run)|*.run|json files (*.json)|*.json|All files (*.*)|*.*";
            dialog.InitialDirectory = Application.StartupPath;
            dialog.Title = "Select a JSON file";
        }

        /// <summary>
        /// Launches About JSONViewer dialog box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Help > About JSON Viewer</remarks>
        private void aboutJSONViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Selects all text in the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Edit > Select All</remarks>
        /// <summary>
        /// Deletes selected text in the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Edit > Delete</remarks>
        /// <summary>
        /// Pastes text in the clipboard into the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Edit > Paste</remarks>
        /// <summary>
        /// Copies text in the textbox into the clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Edit > Copy</remarks>
        /// <summary>
        /// Cuts selected text from the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Edit > Cut</remarks>
        /// <summary>
        /// Undo the last action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Edit > Undo</remarks>
        /// <summary>
        /// Displays the find prompt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Viewer > Find</remarks>
        /// <summary>
        /// Expands all the nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Viewer > Expand All</remarks>
        /// <!---->
        /// <summary>
        /// Copies a node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Viewer > Copy</remarks>
        /// <summary>
        /// Copies just the node's value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Menu item Viewer > Copy Value</remarks>
        /// <!-- JsonViewerTreeNode had to be made public to be accessible here -->
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
        }

        private void explorerTree1_PathChanged(object sender, EventArgs e)
        {
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRun_Click(sender, e);
        }

        private void LoadFromDatabase()
        {
            if (string.IsNullOrEmpty(cboProfile.Text)) return;
            tableLayoutPanel18.Enabled = false;
            listView2.Items.Clear();
            Application.DoEvents();
            //
            string conn_str = "Data Source=" + txtDBServer.Text + ";Initial Catalog=" + txtDB.Text +
            ";Persist Security Info=True;User ID=" + txtUserName.Text + ";Password=" + txtPassword.Text ;
            SqlConnection conn = new SqlConnection(conn_str);
            SqlCommand cmd;
            SqlDataReader dbReader = null;

            string sql = "SELECT Top(" + cboRows.Text + ") [MessageID], CONVERT(varchar(500), [IncomingMessageDateTime], 102) + ' ' + ";
            sql += "CONVERT(varchar(500), [IncomingMessageDateTime], 108) IncomingMessageDateTime, EndPoint, dbo.CHARINDEX3('/', endpoint, 1) ";
            sql += "Service, dbo.CHARINDEX3('/', endpoint, 2) Instance,[EmpCD],[FromIP] Client,[ToIP] Server,[SessionID], ";
            sql += "[TokenID],[Status],[EntityID],[TotalTimeMillSec],[Messages],[StoreCD],[Banner] FROM [dbo].[TraceLog] WHERE 1 = 1";
            //
            string filters = LoadQueryFilters(true, true);
            //
            sql += filters + " ORDER BY MessageID Desc";

            cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 600;
            try
            {
                conn.Open();
                dbReader = cmd.ExecuteReader();

                listView2.Sorting = System.Windows.Forms.SortOrder.None;
                listView2.ListViewItemSorter = null;
                listView2.ShowItemToolTips = true;

                foreach (ColumnHeader ch in listView2.Columns)
                {
                    ch.Text = ch.Text.Replace("> ", "").Replace("< ", "");
                }
                //
                while (dbReader.Read())
                {
                    ListViewItem lvi = new ListViewItem(new[] {
                    dbReader["Endpoint"].ToString().Split('/').Length > 3?
                    dbReader["Endpoint"].ToString().Split('/')[3]:dbReader["Endpoint"].ToString(),
                    dbReader["IncomingMessageDateTime"].ToString().Substring(5, 5),
                    dbReader["IncomingMessageDateTime"].ToString().Substring(10),
                    dbReader["EntityID"].ToString().Split(',')[0],
                });
                    lvi.Tag = dbReader["MessageID"].ToString();
                    if (dbReader["Status"].ToString() == "1")
                        lvi.BackColor = Color.LightPink;

                    listView2.Items.Add(lvi);
                }

                foreach (ColumnHeader column in listView2.Columns)
                {
                    column.Width = -2;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (dbReader != null) dbReader.Close();
                conn.Close();
                conn.Dispose();
                tableLayoutPanel18.Enabled = true;
            }
        }

        private string LoadQueryFilters(bool date_filter, bool other_filters)
        {
            string filters = "";
            if (other_filters)
            { 
                if (cboStatus.SelectedIndex > 0)
                {
                    filters += " AND Status = " + (cboStatus.SelectedIndex - 1);
                }
                //
                if (!string.IsNullOrEmpty(txtEndpoint.Text))
                {
                    filters += " AND Endpoint LIKE '%" + txtEndpoint.Text + "%'";
                }
                //
                if (!string.IsNullOrEmpty(txtEntity.Text))
                {
                    filters += " AND EntityID LIKE '%" + txtEntity.Text + "%'";
                }
            }
            //
            if (date_filter)
            {
                string dateFrom = "";
                string dateTo = "";
                if (tmFrom.Checked)
                {
                    dateFrom = (dtFrom.Value.Date.Ticks + tmFrom.Value.TimeOfDay.Ticks).ToString("X").Insert(10, "-").Insert(5, "-") + "-00-00000";
                    dateTo = (dtFrom.Value.Date.Ticks + tmTo.Value.TimeOfDay.Ticks).ToString("X").Insert(10, "-").Insert(5, "-") + "-00-00000";
                    filters += " AND MessageID BETWEEN '" + dateFrom + "' AND '" + dateTo + "'";
                }
                else
                {
                    dateFrom = (dtFrom.Value.Date.Ticks).ToString("X").Insert(10, "-").Insert(5, "-") + "-00-00000";
                    dateTo = (dtFrom.Value.Date.Ticks + 864000000000).ToString("X").Insert(10, "-").Insert(5, "-") + "-00-00000";
                    filters += " AND MessageID BETWEEN '" + dateFrom + "' AND '" + dateTo + "'";
                }
            }
            //
            return filters;
        }


        private Hashtable LoadFromDatabase(string MessageID)
        {
            if (string.IsNullOrEmpty(cboProfile.Text)) return null;
            string conn_str = "Data Source=" + txtDBServer.Text + ";Initial Catalog=" + txtDB.Text +
            ";Persist Security Info=True;User ID=" + txtUserName.Text + ";Password=" + txtPassword.Text;
            SqlConnection conn = new SqlConnection(conn_str);
            SqlCommand cmd;
            SqlDataReader dbReader = null;

            string sql = "SELECT Top(" + cboRows.Text + ") * FROM [dbo].[TraceLog] ";
            sql += " WHERE MessageID = '" + MessageID + "'";
            //
            cmd = new SqlCommand(sql, conn);

            try
            {
                conn.Open();
                dbReader = cmd.ExecuteReader();
                if (dbReader.Read())
                {
                    Hashtable dr = new Hashtable();
                    dr.Add("IncomingMessage", dbReader["IncomingMessage"].ToString());
                    dr.Add("OutgoingMessage", dbReader["OutgoingMessage"].ToString());
                    return dr;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (dbReader != null) dbReader.Close();
                conn.Close();
                conn.Dispose();
            }
            return null;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            LoadFromDatabase();
            RemoveFocus();
        }

        private void RemoveFocus()
        {
            foreach (Control tb in tableLayoutPanel18.Controls)
                if (tb is ComboBox)
                    ((ComboBox)tb).SelectionLength = 0;
        }

        private void listView2_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count == 0) return;
            string MessageID = listView2.SelectedItems[0].Tag.ToString();
            if (String.IsNullOrEmpty(MessageID)) return;
            //
            Hashtable dr = LoadFromDatabase(MessageID);
            if (dr == null) return;
            string Json_Input = dr["IncomingMessage"].ToString();
            txtJson.Text = Decrypt(Json_Input);
            //
            string Json_Output = dr["OutgoingMessage"].ToString(); ;
            txtResult.Text = Decrypt(Json_Output);
            //
            listView2.Focus();
            //
        }

        public string Decrypt(string txt_Json)
        {
            string clear_text_list = "";
            string key = "Ti9HUVpnTW1Nc1RLK0xBemNMVis1Zz09LGV1TmhYZ21TSFhtMXNuSElmQlFIMlE9PQ==";

            try
            {
                if (!string.IsNullOrEmpty(txt_Json))
                {
                    try
                    {
                        string json = Decrypt(txt_Json, key, 128);
                        clear_text_list = Format(json);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                if (!string.IsNullOrEmpty(clear_text_list))
                    return clear_text_list;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return clear_text_list;
        }

        public string Format(string input)
        {
            try
            {
                JsonSerializer s = new JsonSerializer();
                JsonReader reader = new JsonReader(new StringReader(input));
                Object jsonObject = s.Deserialize(reader);
                if (jsonObject != null)
                {
                    StringWriter sWriter = new StringWriter();
                    JsonWriter writer = new JsonWriter(sWriter);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 4;
                    writer.IndentChar = ' ';
                    s.Serialize(writer, jsonObject);
                    return sWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return input;
        }

        private static string Decrypt(string iEncryptedText, string iCompleteEncodedKey, int iKeySize)
        {
            try
            {
                RijndaelManaged aesEncryption = new RijndaelManaged();
                aesEncryption.KeySize = iKeySize;
                aesEncryption.BlockSize = 128;
                aesEncryption.Mode = CipherMode.CBC;
                aesEncryption.Padding = PaddingMode.PKCS7;
                aesEncryption.IV = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey)).Split(',')[0]);
                aesEncryption.Key = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey)).Split(',')[1]);
                ICryptoTransform decrypto = aesEncryption.CreateDecryptor();
                byte[] encryptedBytes = Convert.FromBase64CharArray(iEncryptedText.ToCharArray(), 0, iEncryptedText.Length);
                return ASCIIEncoding.UTF8.GetString(decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length));
            }
            catch
            {
                throw;
            }
        }

        private void listView2_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Get the new sorting column.
            ColumnHeader new_sorting_column = listView2.Columns[e.Column];

            // Figure out the new sorting order.
            System.Windows.Forms.SortOrder sort_order;
            if (SortingColumn == null)
            {
                // New column. Sort ascending.
                sort_order = System.Windows.Forms.SortOrder.Ascending;
            }
            else
            {
                // See if this is the same column.
                if (new_sorting_column == SortingColumn)
                {
                    // Same column. Switch the sort order.
                    if (SortingColumn.Text.StartsWith("> "))
                    {
                        sort_order = System.Windows.Forms.SortOrder.Descending;
                    }
                    else
                    {
                        sort_order = System.Windows.Forms.SortOrder.Ascending;
                    }
                }
                else
                {
                    // New column. Sort ascending.
                    sort_order = System.Windows.Forms.SortOrder.Ascending;
                }

                // Remove the old sort indicator.
                if (SortingColumn.Text.StartsWith("< ") || SortingColumn.Text.StartsWith("> "))
                    SortingColumn.Text = SortingColumn.Text.Substring(2);
            }

            // Display the new sort order.
            SortingColumn = new_sorting_column;
            if (sort_order == System.Windows.Forms.SortOrder.Ascending)
            {
                SortingColumn.Text = "> " + SortingColumn.Text;
            }
            else
            {
                SortingColumn.Text = "< " + SortingColumn.Text;
            }

            // Create a comparer.
            listView2.ListViewItemSorter =
                new ListViewComparer(e.Column, sort_order);

            // Sort.
            listView2.Sort();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboProfile.Text.ToUpper())
            {
                case "MPOS-LIVE":
                    txtDBServer.Text = "LFLSQL02";
                    txtDB.Text = "ECOMM";
                    txtUserName.Text = "lflsql02-app";
                    txtPassword.Text = "Leons_2022!";
                    break;
                case "ECOM-LIVE":
                    txtDBServer.Text = "LFLSQL01";
                    txtDB.Text = "ECOM_DB";
                    txtUserName.Text = "lflsql01-app";
                    txtPassword.Text = "Leons_2022!";
                    break;
                case "ECOM-DEV":
                    txtDBServer.Text = "ECOMD1";
                    txtDB.Text = "MWSDB";
                    txtUserName.Text = "sa";
                    txtPassword.Text = "Pass123";
                    break;
                case "MPOS-DEV":
                    txtDBServer.Text = "ECOMD1";
                    txtDB.Text = "MWSDB2";
                    txtUserName.Text = "sa";
                    txtPassword.Text = "Pass123";
                    break;
                case "MPOS-UAT":
                    txtDBServer.Text = "ECOMD1";
                    txtDB.Text = "MWSDB3";
                    txtUserName.Text = "sa";
                    txtPassword.Text = "Pass123";
                    break;
            }
            listView2.Items.Clear();
            txtJson.Text = "";
            txtResult.Text = "";
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            LoadFromDatabase();
            RemoveFocus();
        }


        private void listView2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtEndpoint.Text = listView2.SelectedItems[0].Text;
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            restartPCToolStripMenuItem.Enabled = false;
            browseToolStripMenuItem.Enabled = false;
            if (e.Button == MouseButtons.Right && e.Node.Level == 1)
                restartPCToolStripMenuItem.Enabled = true;
            if (e.Button == MouseButtons.Right && e.Node.Level == 2)
                browseToolStripMenuItem.Enabled = true;
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            RemoveFocus();
        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            if (dtFrom.Value.Date > DateTime.Today.Date)
                dtFrom.Value = DateTime.Today.Date;
            /*
            if (dtFrom.Value.AddDays(7) < dtTo.Value)
                dtFrom.Value = dtTo.Value.AddDays(-7);
            */
        }

        private void JsonViewer_ControlLoaded(object sender, EventArgs e)
        {

        }

        private void txtEntity_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tmFrom_ValueChanged(object sender, EventArgs e)
        {
            tmTo.Checked = tmFrom.Checked;
        }

        private void tmTo_ValueChanged(object sender, EventArgs e)
        {
            tmFrom.Checked = tmTo.Checked;
        }
    }

    public class base_response
    {
        public bool Successful;
    }

    public class ExtendedWebClient : System.Net.WebClient
    {

        private int timeout;
        public int Timeout
        {
            get
            {
                return timeout;
            }
            set
            {
                timeout = value;
            }
        }
        public ExtendedWebClient(Uri address)
        {
            this.timeout = 600000;//In Milli seconds
            var objWebClient = GetWebRequest(address);
        }
        protected override WebRequest GetWebRequest(Uri address)
        {
            var objWebRequest = base.GetWebRequest(address);
            objWebRequest.Timeout = this.timeout;
            return objWebRequest;
        }
    }
}
